//
//  JSONObject.swift
//  Weather Friend
//
//  Created by David Velarde on 7/10/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

typealias JSONDict = [AnyHashable: Any]
typealias JSON = [JSONValue]

public indirect enum JSONType {
	case number(value: NSNumber)
	case string(value: String)
	case bool(value: Bool)
	case dictionary(value: [JSONValue])
	case array(value: [JSONType])
	case null
	case undefined
	
	
	var stringValue: String {
		switch self {
		case .string(let value):
			return value
		default:
			return ""
		}
	}
	var intValue: Int {
		switch self {
			case .number(let value):
			return value.intValue
			default:
			return 0
		}
	}
	var boolValue: Bool {
		switch self {
		case .bool(let value):
			return value
		default:
			return false
		}
	}
	var doubleValue: Double {
		switch self {
		case .number(let value):
			return value.doubleValue
		default:
			return 0
		}
	}
	var arrayValue: [JSONType] {
		switch self {
		case .array(let value):
			return value
		default:
			return [JSONType]()
		}
	}
	var dictionaryValue: JSON {
		switch self {
		case .dictionary(let value):
			return value
		default:
			return JSON()
		}
	}
	var isNull: Bool {
		switch self {
		case .null:
			return true
		case .array:
			if arrayValue.isEmpty {
				return true
			} else {
				return false
			}
		case .dictionary:
			if dictionaryValue.isEmpty {
				return true
			} else {
				return false
			}
		default:
			return false
		}
	}
	
}

public struct JSONValue {
	internal let key: String!
	public let value: JSONType!
}


struct JSONParser {
	
	static func getJSONType(object: Any) -> JSONType {
		var jsonValue: JSONType
		
		switch object {
		
		case let number as NSNumber:
			if object is Double || object is Int {
				jsonValue = .number(value: number)
			} else {
				jsonValue = .bool(value: number.boolValue)
			}
		case let string as String:
			jsonValue = .string(value: string)
		case _ as NSNull:
			fallthrough
		case nil:
			jsonValue = .null
		case let rawArray as [Any]:
			let array = rawArray.map({ convertible -> JSONType in
				getJSONType(object: convertible)
			})
			jsonValue = .array(value: array)
		case let dictionary as JSONDict:
			let jsonObject = JSONParser.parse(dictionary)
			jsonValue = .dictionary(value: jsonObject)
		default:
			jsonValue = .undefined
		}
		
		return jsonValue
	}
	
	private var data = [JSONValue]()
	
	static func parse(_ literal: JSONDict) -> [JSONValue] {
		return literal.map { (key, value) -> JSONValue in
			let jsonEntry = JSONValue(key: key as! String, value: JSONParser.getJSONType(object: value))
			return jsonEntry
		}
	}
	
	
}

extension Array where Element == JSONValue {
	subscript(key: String) -> [JSONValue] {
		get {
			let entries = self.filter { (value) -> Bool in
				value.key == key
			}
			if entries.count > 0 {
				let type = entries.first!.value
				switch type! {
				case .dictionary(let value):
					return value
				default:
					return [entries.first!]
				}
			} else {
				return [JSONValue]()
			}
		}
	}
	
	func validValue() -> JSONType? {
			return self.first?.value
	}
	
	var stringValue: String {
		if let value = validValue() {
			switch value {
			case .string(let value):
				return value
			default:
				return ""
			}
		} else {
			return ""
		}
	}
	var intValue: Int {
		if let value = validValue() {
			switch value {
			case .number(let value):
				return value.intValue
			default:
				return 0
			}
		} else {
			return 0
		}
	}
	var boolValue: Bool {
		if let value = validValue() {
			switch value {
			case .bool(let value):
				return value
			default:
				return false
			}
		} else {
			return false
		}
	}
	var doubleValue: Double {
		if let value = validValue() {
			switch value {
			case .number(let value):
				return value.doubleValue
			default:
				return 0
			}
		} else {
			return 0
		}
	}
	var arrayValue: [JSONType] {
		if let value = validValue() {
			switch value {
			case .array(let value):
				return value
			default:
				return [JSONType]()
			}
		} else {
			return [JSONType]()
		}
	}
	var dictionaryValue: JSON {
		if let value = validValue() {
			switch value {
			case .dictionary(let value):
				return value
			default:
				return [JSONValue]()
			}
		} else {
			return [JSONValue]()
		}
	}
	var isNull: Bool {
		if let value = validValue() {
			switch value {
			case .null:
				return true
			case .array:
				if arrayValue.isEmpty {
					return true
				} else {
					return false
				}
			case .dictionary:
				if dictionaryValue.isEmpty {
					return true
				} else {
					return false
				}
			default:
				return false
			}
		} else {
			return true
		}
	}
}
