//
//  CityWeatherViewModel.swift
//  Weather Friend
//
//  Created by David Velarde on 7/10/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

public struct CityViewModel {
	public let cityName: String
	public let latitude: Double
	public let longitude: Double
	public var entry: WeatherEntry? = nil
	
	public init(withBMLocation location: BMLocation) {
		cityName = location.locationName
		latitude = location.lat
		longitude = location.lon
	}
	
}
