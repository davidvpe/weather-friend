//
//  UserSettings.swift
//  Weather Friend
//
//  Created by David Velarde on 7/10/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

public struct UserSettings {
	
	enum UserSettingsKeys: String {
		case system
		case arrayLocations
	}
	
	static func getStringValue(key:String) -> String {
		if let value = UserDefaults.standard.object(forKey: key) as? String {
			return value
		} else {
			return ""
		}
	}
	
	static func setStringValue(key:String, value: Any) {
		UserDefaults.standard.set(value, forKey: key)
		UserDefaults.standard.synchronize()
	}
	
	static var savedLocations: [BMLocation] {
		get {
			if let data = UserDefaults.standard.data(forKey: UserSettingsKeys.arrayLocations.rawValue),
				let arrayLocations = NSKeyedUnarchiver.unarchiveObject(with: data) as? [BMLocation] {
				return arrayLocations
			} else {
				return [BMLocation]()
			}
		}
		
		set {
			let data = NSKeyedArchiver.archivedData(withRootObject: newValue)
			UserDefaults.standard.set(data, forKey: UserSettingsKeys.arrayLocations.rawValue)
			UserDefaults.standard.synchronize
		}
	}
	
	static var currentSystem: UnitSystem {
		get {
			if let system = UnitSystem(rawValue: getStringValue(key: UserSettingsKeys.system.rawValue)) {
				return system
			} else {
				return .metric
			}
		}
		set {
			setStringValue(key: UserSettingsKeys.system.rawValue, value: newValue.rawValue)
		}
	}
}
