//
//  String.swift
//  Weather Friend
//
//  Created by David Velarde on 7/10/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

public extension NumberFormatter {
	
	public static func standarFormatter() -> NumberFormatter {
		let formatter = NumberFormatter()
		formatter.decimalSeparator = "."
		formatter.minimumIntegerDigits = 1
		formatter.maximumFractionDigits = 1
		return formatter
	}
	
}
