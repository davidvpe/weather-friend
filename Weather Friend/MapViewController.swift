//
//  MapViewController.swift
//  Weather Friend
//
//  Created by David Velarde on 7/11/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit
import MapKit
import CoreWeatherFriend

class MapViewController: UIViewController {

	@IBOutlet weak var navigationBar: UINavigationBar!
	@IBOutlet weak var imgMarker: UIImageView!
	@IBOutlet weak var btnSave: UIBarButtonItem!
	var isFetching = false {
		didSet {
			if isFetching {
				let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
				spinner.startAnimating()
				navigationItem.titleView = spinner
			} else {
				navigationItem.titleView = nil
			}
		}
	}
	
	var selectedCoordinate = CLLocationCoordinate2D(latitude: 0, longitude: 0)
	var weatherEntry: WeatherEntry?
	
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
	
	func fadeMarkerIn() {
		UIView.animate(withDuration: 0.2) {
			self.imgMarker.frame.origin.y += 10
			self.imgMarker.alpha = 1
		}
	}
	
	func fadeMarkerOut() {
		UIView.animate(withDuration: 0.2) {
			self.imgMarker.frame.origin.y -= 10
			self.imgMarker.alpha = 0.5
		}
	}
	
	func obtainLocationInfo() {
		
		isFetching = true
		
		let operation = Store.shared.getOperationWeatherFor(latitude: selectedCoordinate.latitude, longitude: selectedCoordinate.longitude, completionBlock: { entry in
			DispatchQueue.main.async {
				self.isFetching = false
				switch entry {
					case .value(let value):
						self.weatherEntry = value
						self.btnSave.isEnabled = true
						self.navigationBar.items!.first!.title = "\(value.placeName) ( \(value.temperature.string) )"
						
					default:
						break
				}
			}
		})
		
		Store.shared.enqueueOperationWithDependancies(operation: operation)
		
	}
	
	@IBAction func onDone(_ sender: UIBarButtonItem) {
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func onSave(_ sender: UIBarButtonItem) {
		if let entry = weatherEntry {
			if entry.placeName == "" {
				self.presentAlert(title: "Oops", message: "Although you pointed a location, this appears to be invalid. Please choose another one.")
			} else {
				
				let savedLocation = BMLocation(name: entry.placeName, lat: selectedCoordinate.latitude, lon: selectedCoordinate.longitude)
				if DataManager.shared.save(newLocation: savedLocation) {
					self.presentAlert(title: "Success", message: "You've successfully added \(entry.placeName) as a bookmarked location")
				} else {
					self.presentAlert(title: "Oops", message: "\(entry.placeName) is already a bookmarked location")
				}
			}
		}
		
	}

}

extension MapViewController: MKMapViewDelegate {
	
	func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
		selectedCoordinate = mapView.centerCoordinate
	}
	
	func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
		btnSave.isEnabled = false
		fadeMarkerOut()
	}
	func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
		fadeMarkerIn()
		selectedCoordinate = mapView.centerCoordinate
		obtainLocationInfo()
		
	}
}
