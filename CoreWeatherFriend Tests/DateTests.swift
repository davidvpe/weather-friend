//
//  DateTests.swift
//  Weather Friend
//
//  Created by David Velarde on 7/13/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import XCTest

class DateTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
	
	func testEverything() {
		let date = Date(timeIntervalSince1970: 1399980366)
		
		XCTAssert(date.shortString == "5/13/14, 6:26 AM", "Wrong Date Format")
		XCTAssert(date.dayName == "Tuesday", "Wrong Day Name")
		XCTAssert(date.monthDay == "May 13", "Wrong Month Name")
		XCTAssert(date.mediumString == "Tuesday, May 13", "Wrong Date Name")
		XCTAssert(date.onlyHour == "6:26 AM", "Wrong Hour Format")
		XCTAssert(date.tomorrow == date.addingTimeInterval(TimeInterval(3600*24.0)), "Wrong Tomorrow Function")
		XCTAssert(date.isSameDay(otherDate: date), "Wrong is Same Day")
		XCTAssert(!date.isSameDay(otherDate: Date()), "Wrong is Same Day")
		XCTAssert(!date.isToday, "Wrong is today")
	}
}
