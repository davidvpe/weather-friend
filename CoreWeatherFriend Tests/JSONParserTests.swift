//
//  JSONParser.swift
//  Weather Friend
//
//  Created by David Velarde on 7/13/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import XCTest
@testable import CoreWeatherFriend

class JSONParserTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
	func testInitialization() {
		
		let jsonDict: JSONDict
		
		jsonDict = ["keyString":"value",
		            "keyNumber":1,
		            "keyBool":true,
		            "keyDouble":1.2,
		            "keyArrayNumbers":[1,2,3,4],
		            "keyArrayStrings":["a","b","c","d"],
		            "keyArrayDictionarys":[
						["key":"dict","number":1,"arrayBools":[true,false]],
						["key":"dict","number":2]
					],
					"keyDictionary":["key":"dict","number":2]
		]
		
		let json = JSONParser.parse(jsonDict)
		
		XCTAssert(json["keyString"].stringValue == "value", "Couldn't extract string")
		XCTAssert(json["keyNumber"].intValue == 1, "Couldn't extract int")
		XCTAssert(json["keyDouble"].doubleValue == 1.2, "Couldn't extract double")
		XCTAssert(json["keyBool"].boolValue == true, "Couldn't extract bool")
		
		let arrayNumbers = json["keyArrayNumbers"].arrayValue
		let arrayStrings = json["keyArrayStrings"].arrayValue
		XCTAssert(arrayNumbers[0].intValue == 1, "Couldn't extract number from array")
		XCTAssert(arrayStrings[0].stringValue == "a", "Couldn't extract number from array")
		let arrayDictionary = json["keyArrayDictionarys"].arrayValue
		let firstDictionary = arrayDictionary.first!.dictionaryValue
		let arrayBools = firstDictionary["arrayBools"].arrayValue
		
		XCTAssert(arrayBools.first?.boolValue == true, "Couldn't extract bool from array of dictionaries")
		XCTAssert(arrayNumbers.count == 4, "Couldn't extract array")
		
		
	}
		
}
