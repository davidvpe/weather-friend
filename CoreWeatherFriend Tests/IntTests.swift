//
//  IntTests.swift
//  Weather Friend
//
//  Created by David Velarde on 7/13/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import XCTest

class IntTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testIsBetween() {
		XCTAssert(10.isBetween(lower: 10, upper: 11), "Is Between including not working correctly")
		XCTAssert(11.isBetween(lower: 10, upper: 11), "Is Between including not working correctly")
		XCTAssert(11.isBetween(lower: 11, upper: 11), "Is Between including not working correctly")
		XCTAssert(!11.isBetween(lower: 11, upper: 11, including: false), "Is Between not including not working correctly")
		XCTAssert(!10.isBetween(lower: 11, upper: 11, including: false), "Is Between not including not working correctly")
		XCTAssert(10.isBetween(lower: 9, upper: 11, including: false), "Is Between not including not working correctly")
		XCTAssert(!10.isBetween(lower: 12, upper: 11, including: false), "Is Between not including not working correctly")
    }
	
}
